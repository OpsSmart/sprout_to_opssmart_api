# Sprout to OpsSmart API Document

The documentation is made to describe data transfer method between Sprout and OpsSmart.

## Method POST

### Request

`URL` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; `https://opssmarttrace.com/SoliPackHouse/EDEAPI/LEGACY_IMPORT` <br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API URL (will be changed to new URL when the project go live)

**Authorization** Bearer Token

| Key | Value |
| ------ | ------ |
| Token | xxx |

`Token` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; encrypted Bearer Token provided by OpsSmart (OpsSmart API Token)

**Request Headers**

| Key | Value |
| ------ | ------ |
| UserName | xxx |
| Password | xxx |
| APIFormat | JSON or XML |

`UserName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API User Name who registered in OpsSmart

`Password` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API Password

`APIFormat` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; schema type of request body can be only `JSON` for now

### Body

```json
{
    "Document": {
        "RequestDateTime": "2023-03-28 13:38:56:607",
        "Sender": "Sprout",
	"SessionID": "098f6bcd4621d373cade4e832627b4f6",
        "ActionType": "REALTIME",
	
        "DataObject": [ 
		"TableName": "sales_orders",
                "TotalRecord": "2",
                "DataSets": {
                    "DataRecord": [
                        {
                            "RowID": 1,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "id",
                                        "Value": "649208"
                                    },
                                    {
                                        "Name": "receivable_invoice_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "company_site_id",
                                        "Value": "7"
                                    },
                                    {
                                        "Name": "customer_id",
                                        "Value": "67"
                                    },
                                    {
                                        "Name": "customer_site_id",
                                        "Value": "242"
                                    },
                                    {
                                        "Name": "order_total",
                                        "Value": "14107.50"
                                    },
                                    {
                                        "Name": "delivery_date",
                                        "Value": "2023-03-13 00:00" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "pack_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "order_date",
                                        "Value": "2023-03-11 00:00" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "customer_route_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "stop_code",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "pick_ticket_prints",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "sales_order_prints",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "status-sprout",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "sales_order_type_id",
                                        "Value": "4"
                                    },
                                    {
                                        "Name": "pick_ticket_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "invoice_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "internal_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "customer_reference_text",
                                        "Value": "N326936"
                                    },
                                    {
                                        "Name": "subcontractor_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "created_at",
                                        "Value": "2023-02-06 21:07" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "updated_at",                                        
                                        "Value": "2023-03-07 12:10" //"yyyy-MM-dd HH:mm"
                                    }
                                ]
                            }
                        },
                        {
                            "RowID": 2,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "id",
                                        "Value": "649670"
                                    },
                                    {
                                        "Name": "receivable_invoice_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "company_site_id",
                                        "Value": "5"
                                    },
                                    {
                                        "Name": "customer_id",
                                        "Value": "43"
                                    },
                                    {
                                        "Name": "customer_site_id",
                                        "Value": "103"
                                    },
                                    {
                                        "Name": "order_total",
                                        "Value": "4720.20"
                                    },
                                    {
                                        "Name": "delivery_date",
                                        "Value": "2023-03-07 00:00" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "pack_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "order_date",
                                        "Value": "2023-03-06 00:00"
                                    },
                                    {
                                        "Name": "customer_route_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "stop_code",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "pick_ticket_prints",
                                        "Value": "3"
                                    },
                                    {
                                        "Name": "sales_order_prints",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "status",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "sales_order_type_id",
                                        "Value": "1"
                                    },
                                    {
                                        "Name": "pick_ticket_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "invoice_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "internal_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "customer_reference_text",
                                        "Value": "205879"
                                    },
                                    {
                                        "Name": "subcontractor_id",
                                        "Value": "0"
                                    },
                                    {
                                        "Name": "created_at",
                                        "Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "updated_at",                                        
                                        "Value": "2023-03-06 19:11" //"yyyy-MM-dd HH:mm"
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                "TableName": "sales_order_line_items",                
                "TotalRecord": "2",
                "DataSets": {
                    "DataRecord": [
                        {
                            "RowID": 1,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "id",
                                        "Value": "480100"
                                    },
                                    {
                                        "Name": "sales_order_id",
                                        "Value": "649670"
                                    },
                                    {
                                        "Name": "item_id",
                                        "Value": "752"
                                    },
                                    {
                                        "Name": "description",
                                        "Value": "OG Rosemary .66oz 3pk OO"
                                    },
                                    {
                                        "Name": "pick_ticket_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "invoice_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "internal_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "customer_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "quantity_ordered",
                                        "Value": "80.00"
                                    },
                                    {
                                        "Name": "quantity_shipped",
                                        "Value": "80.00"
                                    },
                                    {
                                        "Name": "unit_of_measure_id",
                                        "Value": "10"
                                    },
                                    {
                                        "Name": "company_site_id",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "country_of_origin_id",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "cost",
                                        "Value": "0.00"
                                    },
                                    {
                                        "Name": "price",
                                        "Value": "2.48"
                                    },
                                    {
                                        "Name": "last_price",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "extended",
                                        "Value": "198.40"
                                    },
                                    {
                                        "Name": "last_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "pack_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "created_at",
                                        "Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "updated_at",
                                        "Value": "2023-03-06 14:31" //"yyyy-MM-dd HH:mm"
                                    }
                                ]
                            }
                        },
                        {
                            "RowID": 2,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "id",
                                        "Value": "480101"
                                    },
                                    {
                                        "Name": "sales_order_id",
                                        "Value": "649670"
                                    },
                                    {
                                        "Name": "item_id",
                                        "Value": "741"
                                    },
                                    {
                                        "Name": "description",
                                        "Value": "OG Basil .66oz 3pk OO"
                                    },
                                    {
                                        "Name": "pick_ticket_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "invoice_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "internal_note",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "customer_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "source_reference_text",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "quantity_ordered",
                                        "Value": "320.00"
                                    },
                                    {
                                        "Name": "quantity_shipped",
                                        "Value": "320.00"
                                    },
                                    {
                                        "Name": "unit_of_measure_id",
                                        "Value": "10"
                                    },
                                    {
                                        "Name": "company_site_id",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "country_of_origin_id",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "cost",
                                        "Value": "0.00"
                                    },
                                    {
                                        "Name": "price",
                                        "Value": "2.48"
                                    },
                                    {
                                        "Name": "last_price",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "extended",
                                        "Value": "793.60"
                                    },
                                    {
                                        "Name": "last_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "pack_date",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "created_at",
                                        "Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
                                    },
                                    {
                                        "Name": "updated_at",
                                        "Value": "2023-03-06 14:31" //"yyyy-MM-dd HH:mm"
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]        
    }
}
```

`Document` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of all Objects

`RequestDateTime` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Datetime while sending the request, format `yyyy/MM/dd HH:mm:ss:fff`

`Sender` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; User Name of Sender

`ActionType` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Fixed Value (REALTIME)

`DataObject` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of Sprout Database Table

`TableName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Sprout Database Table Name

`TotalRecord` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Total Record to transfer

`DataSets` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of DataRecord

`DataRecord`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of every records in the Sprout Table

`RowID`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Record Row Number

`Attribute`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Name = "Sprout Column Name", Value = Actual Data

### Response
```json
{
  "Document": {
    "ResponseDateTime": "2021-10-26 10:22:15:509",
    "Sender": "OpsSmart",
    "SessionID": "098f6bcd4621d373cade4e832627b4f6",
    "ActionType": "REALTIME",
    
    "DataObject": [ 
		"TableName": "sales_orders",
		"TotalRecord": "2",
		"DataSets": {
		    "DataRecord": [
			{
			    "RowID": 1,
			    "Attributes": {
				"Attribute": [
				    {
					"Name": "id",
					"Value": "649208"
				    },
				    {
					"Name": "receivable_invoice_id",
					"Value": "0"
				    },
				    {
					"Name": "company_site_id",
					"Value": "7"
				    },
				    {
					"Name": "customer_id",
					"Value": "67"
				    },
				    {
					"Name": "customer_site_id",
					"Value": "242"
				    },
				    {
					"Name": "order_total",
					"Value": "14107.50"
				    },
				    {
					"Name": "delivery_date",
					"Value": "2023-03-13 00:00" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "pack_date",
					"Value": ""
				    },
				    {
					"Name": "order_date",
					"Value": "2023-03-11 00:00" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "customer_route_id",
					"Value": "0"
				    },
				    {
					"Name": "stop_code",
					"Value": "0"
				    },
				    {
					"Name": "pick_ticket_prints",
					"Value": "0"
				    },
				    {
					"Name": "sales_order_prints",
					"Value": "0"
				    },
				    {
					"Name": "status-sprout",
					"Value": "0"
				    },
				    {
					"Name": "sales_order_type_id",
					"Value": "4"
				    },
				    {
					"Name": "pick_ticket_note",
					"Value": ""
				    },
				    {
					"Name": "invoice_note",
					"Value": ""
				    },
				    {
					"Name": "internal_note",
					"Value": ""
				    },
				    {
					"Name": "source",
					"Value": ""
				    },
				    {
					"Name": "source_reference_text",
					"Value": ""
				    },
				    {
					"Name": "customer_reference_text",
					"Value": "N326936"
				    },
				    {
					"Name": "subcontractor_id",
					"Value": "0"
				    },
				    {
					"Name": "created_at",
					"Value": "2023-02-06 21:07" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "updated_at",                                        
					"Value": "2023-03-07 12:10" //"yyyy-MM-dd HH:mm"
				    }
				]
			    }
			},
			{
			    "RowID": 2,
			    "Attributes": {
				"Attribute": [
				    {
					"Name": "id",
					"Value": "649670"
				    },
				    {
					"Name": "receivable_invoice_id",
					"Value": "0"
				    },
				    {
					"Name": "company_site_id",
					"Value": "5"
				    },
				    {
					"Name": "customer_id",
					"Value": "43"
				    },
				    {
					"Name": "customer_site_id",
					"Value": "103"
				    },
				    {
					"Name": "order_total",
					"Value": "4720.20"
				    },
				    {
					"Name": "delivery_date",
					"Value": "2023-03-07 00:00" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "pack_date",
					"Value": ""
				    },
				    {
					"Name": "order_date",
					"Value": "2023-03-06 00:00"
				    },
				    {
					"Name": "customer_route_id",
					"Value": "0"
				    },
				    {
					"Name": "stop_code",
					"Value": "0"
				    },
				    {
					"Name": "pick_ticket_prints",
					"Value": "3"
				    },
				    {
					"Name": "sales_order_prints",
					"Value": "0"
				    },
				    {
					"Name": "status",
					"Value": "0"
				    },
				    {
					"Name": "sales_order_type_id",
					"Value": "1"
				    },
				    {
					"Name": "pick_ticket_note",
					"Value": ""
				    },
				    {
					"Name": "invoice_note",
					"Value": ""
				    },
				    {
					"Name": "internal_note",
					"Value": ""
				    },
				    {
					"Name": "source",
					"Value": ""
				    },
				    {
					"Name": "source_reference_text",
					"Value": ""
				    },
				    {
					"Name": "customer_reference_text",
					"Value": "205879"
				    },
				    {
					"Name": "subcontractor_id",
					"Value": "0"
				    },
				    {
					"Name": "created_at",
					"Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "updated_at",                                        
					"Value": "2023-03-06 19:11" //"yyyy-MM-dd HH:mm"
				    }
				]
			    }
			}
		    ]
		}
	    },
	    {
		"TableName": "sales_order_line_items",                
		"TotalRecord": "2",
		"DataSets": {
		    "DataRecord": [
			{
			    "RowID": 1,
			    "Attributes": {
				"Attribute": [
				    {
					"Name": "id",
					"Value": "480100"
				    },
				    {
					"Name": "sales_order_id",
					"Value": "649670"
				    },
				    {
					"Name": "item_id",
					"Value": "752"
				    },
				    {
					"Name": "description",
					"Value": "OG Rosemary .66oz 3pk OO"
				    },
				    {
					"Name": "pick_ticket_note",
					"Value": ""
				    },
				    {
					"Name": "invoice_note",
					"Value": ""
				    },
				    {
					"Name": "internal_note",
					"Value": ""
				    },
				    {
					"Name": "customer_reference_text",
					"Value": ""
				    },
				    {
					"Name": "source",
					"Value": ""
				    },
				    {
					"Name": "source_reference_text",
					"Value": ""
				    },
				    {
					"Name": "quantity_ordered",
					"Value": "80.00"
				    },
				    {
					"Name": "quantity_shipped",
					"Value": "80.00"
				    },
				    {
					"Name": "unit_of_measure_id",
					"Value": "10"
				    },
				    {
					"Name": "company_site_id",
					"Value": ""
				    },
				    {
					"Name": "country_of_origin_id",
					"Value": ""
				    },
				    {
					"Name": "cost",
					"Value": "0.00"
				    },
				    {
					"Name": "price",
					"Value": "2.48"
				    },
				    {
					"Name": "last_price",
					"Value": ""
				    },
				    {
					"Name": "extended",
					"Value": "198.40"
				    },
				    {
					"Name": "last_date",
					"Value": ""
				    },
				    {
					"Name": "pack_date",
					"Value": ""
				    },
				    {
					"Name": "created_at",
					"Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "updated_at",
					"Value": "2023-03-06 14:31" //"yyyy-MM-dd HH:mm"
				    }
				]
			    }
			},
			{
			    "RowID": 2,
			    "Attributes": {
				"Attribute": [
				    {
					"Name": "id",
					"Value": "480101"
				    },
				    {
					"Name": "sales_order_id",
					"Value": "649670"
				    },
				    {
					"Name": "item_id",
					"Value": "741"
				    },
				    {
					"Name": "description",
					"Value": "OG Basil .66oz 3pk OO"
				    },
				    {
					"Name": "pick_ticket_note",
					"Value": ""
				    },
				    {
					"Name": "invoice_note",
					"Value": ""
				    },
				    {
					"Name": "internal_note",
					"Value": ""
				    },
				    {
					"Name": "customer_reference_text",
					"Value": ""
				    },
				    {
					"Name": "source",
					"Value": ""
				    },
				    {
					"Name": "source_reference_text",
					"Value": ""
				    },
				    {
					"Name": "quantity_ordered",
					"Value": "320.00"
				    },
				    {
					"Name": "quantity_shipped",
					"Value": "320.00"
				    },
				    {
					"Name": "unit_of_measure_id",
					"Value": "10"
				    },
				    {
					"Name": "company_site_id",
					"Value": ""
				    },
				    {
					"Name": "country_of_origin_id",
					"Value": ""
				    },
				    {
					"Name": "cost",
					"Value": "0.00"
				    },
				    {
					"Name": "price",
					"Value": "2.48"
				    },
				    {
					"Name": "last_price",
					"Value": ""
				    },
				    {
					"Name": "extended",
					"Value": "793.60"
				    },
				    {
					"Name": "last_date",
					"Value": ""
				    },
				    {
					"Name": "pack_date",
					"Value": ""
				    },
				    {
					"Name": "created_at",
					"Value": "2023-02-09 21:10" //"yyyy-MM-dd HH:mm"
				    },
				    {
					"Name": "updated_at",
					"Value": "2023-03-06 14:31" //"yyyy-MM-dd HH:mm"
				    }
				]
			    }
			}
		    ]
		}
	    }
	],

	"ResponseCode": "0000",
	"ResponseDesc": "Process successfully completed."
  }
}
```

**Response Code**

| RESPONSE_CODE | DESCRIPTION |
| ------ | ------ |
| 0000 | The process was successfully completed. |
| OPS2001 | Authentication failed. Invalid API Username, Password or Token. |
| OPS2002 | Authentication failed. The API Token expired. |
| OPS2003 | Invalid API Request message format. Failed to retrieve OpsSmart Table. TABLE_NAME = [[TABLE_NAME]] |
| OPS2004 | Invalid API Request message format. Failed to match Sprout Column with OpsSmart. COLUMN_NAME = [[COLUMN_NAME]] |
| OPS2005 | Invalid API Request message format. |
| OPS2006 | Mandatory field required. |
| OPS2010 | DataSet [[TABLE_NAME]] data does not match with OpsSmart DataSet |
| OPS8000 | Some records failed to import. |
| OPS9000 | No record found. TABLE_NAME = [[TABLE_NAME]] |
| OPS9999 | Data Import process failed. |


**Sprout Tables**

| No. | TABLE NAME | RELATIONSHIP |
| ------ | ------ | ------ |
| 1 | company_sites | |
| 2 | country_of_origins | |
| 3 | customer_sites | |
| 4 | customers | |
| 5 | inventory_allocation_types | |
| 6 | inventory_allocations | |
| 7 | inventory_receipt_types | |
| 8 | inventory_receipts | |
| 9 | inventory_transfer_receipts | |
| 10 | inventory_transfer_status_details | |
| 11 | item_group_types | |
| 12 | item_groups | |
| 13 | item_item_group | |
| 14 | items | |
| 15 | kit_recipe_component_items | |
| 16 | kit_recipes | |
| 17 | label_runs | |
| 18 | pack_assignments | |
| 19 | printers | |
| 20 | production_tables | |
| 21 | purchase_order_sets | |
| 22 | purchase_order_status_details | |
| 23 | receivable_invoice_types | |
| 24 | receivable_invoices | |
| 25 | sales_order_status_details | |
| 26 | sales_order_types | |
| 27 | unit_of_measures | |
| 28 | vendors | |
|  |  |  |
| 29 | inventory_transfers | |
| 30 | inventory_transfer_line_items | inventory_transfers.id = inventory_transfer_line_items.inventory_transfer_id |
|  |  |  |
| 31 | purchase_orders | |
| 32 | purchase_order_line_items | purchase_orders.id = purchase_order_line_items.purchase_order_id |
|  |  |  |
| 33 | sales_orders | |
| 34 | sales_order_line_items | sales_orders.id = sales_order_line_items.sales_order_id |

